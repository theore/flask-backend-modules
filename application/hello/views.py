# -*- coding: utf-8 -*-
import inspect
import logging
import urllib2

from flask import render_template
from flask import request, jsonify, Blueprint
from google.appengine.api import modules

bp_hello = Blueprint('hello', __name__, url_prefix='/hello')


@bp_hello.route('/', methods=['GET'])
def home():
    return render_template('index.html')


@bp_hello.route('/module-info', methods=['GET'])
def module_info():
    # [START module_info]
    module = modules.get_current_module_name()
    instance_id = modules.get_current_instance_id()
    backend_hostname = modules.get_hostname(module='my-backend')
    url = "http://{}/".format(backend_hostname)

    res = {
        'module_id': module,
        'instance_id': instance_id,
        'backend_hostname': backend_hostname,
        'url': url
    }
    return jsonify(res)
    # [END module_info]


@bp_hello.route('/access_backend', methods=['GET'])
def access_backend():
    # [START access_another_module]
    backend_hostname = modules.get_hostname(module='my-backend')
    url = "http://{}/backend_module".format(backend_hostname)
    try:
        result = urllib2.urlopen(url).read()
        res = {'response': result}
        return jsonify(res)
    except urllib2.URLError as e:
        logging.exception(e)

        return 'error'
    # [END access_another_module]

