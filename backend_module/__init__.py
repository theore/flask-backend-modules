# -*- coding: utf-8 -*-
import logging

from flask import current_app, Flask, redirect, url_for
from flask import render_template

import settings


def create_app(config, debug=False, testing=False, config_overrides=None):
    app = Flask(__name__, template_folder=settings.TEMPLATE_FOLDER)
    app.config.from_object(config)

    app.debug = debug
    app.testing = testing

    if config_overrides:
        app.config.update(config_overrides)

    # Configure logging
    if not app.testing:
        logging.basicConfig(level=logging.INFO)

    # Register any blueprint.
    from backend_module.views import bp_backend_module
    app.register_blueprint(bp_backend_module)

    # [START app.route]
    # Add a default root route.
    # [END app.route]

    return app
