# -*- coding: utf-8 -*-
import inspect
import logging

from flask import request, jsonify, Blueprint


bp_backend_module = Blueprint('backend_module', __name__, url_prefix='/backend_module')


@bp_backend_module.route('/', methods=['GET'])
def home():
    return 'Hello Backend.'
